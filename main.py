#!/usr/bin/env python3
import os
import logging
import json
import time
import urllib.request

CACHE_VALUE = None
LAST_CACHE_TIME = 0
CACHE_SAVE_TIME = 3600 # 1 hour

from programaker_bridge import (
    ProgramakerBridge,
    BlockArgument,
    CallbackBlockArgument,
    BlockContext,
)

bridge = ProgramakerBridge(
    name="Steam Store Prices",
    endpoint=os.environ['BRIDGE_ENDPOINT'],
    token=os.getenv('BRIDGE_AUTH_TOKEN', None),
    is_public=False,
)

global APP_LIST

def update_app_list():
    logging.info("Loading applist")
    app_list = {}

    with open("applist.json", 'rt') as f:
        apps = json.load(f)['applist']['apps']
        for app in apps:
            app_list[app['name'].upper().strip()] = app

    global APP_LIST
    APP_LIST = app_list
    logging.info("Loaded {} apps", len(APP_LIST))

update_app_list()

@bridge.callback
def get_steam_fields(_extra_data):
    return (
        {'id': 'price_final_fmt', 'name': 'Current price (formatted: XX.XX€ )'},
        {'id': 'price_final_num', 'name': 'Current price (numeric: XX.XX )'},
        {'id': 'name', 'name': 'Name'},
    )


@bridge.getter(
    id="get_app_data_by_name",
    message="Get %1 %2",
    arguments=[
        BlockArgument(str, "BYTEPATH"),
        CallbackBlockArgument(str, get_steam_fields),
    ],
    block_result_type=str,
)
def get_app_data_by_name(app_name, field, extra_data):
    global LAST_CACHE_TIME, CACHE_SAVE_TIME, CACHE_VALUE
    global APP_LIST

    logging.info("Getting {} data".format(app_name))
    app_name = app_name.upper().strip()
    app_id = str(APP_LIST[app_name]['appid'])
    logging.info("AppId {}->{}".format(app_name, app_id))

    result = urllib.request.urlopen(
        'http://store.steampowered.com/api/appdetails?appids=' + app_id
    ).read()
    
    CACHE_VALUE = json.loads(result)[app_id]['data']

    if field == 'name':
        return CACHE_VALUE['name']
    elif field == 'price_final_fmt':
        return CACHE_VALUE['price_overview']['final_formatted']
    elif field == 'price_final_num':
        return CACHE_VALUE['price_overview']['final'] / 100.


if __name__ == '__main__':
   logging.basicConfig(format="%(asctime)s - %(levelname)s [%(filename)s] %(message)s")
   logging.getLogger().setLevel(logging.INFO)

   logging.info('Starting bridge')
   bridge.run()
